What I have deployed from the ansible side(testing phase):
- Two directories ansible and sandbox
- Installed ansible locally and added localhost to the host list. The file at ./ansible/prerequisites/ansible.hosts is the same as my local /etc/ansible/hosts
- Wrote an ansible-playbook called ./ansible/deploy_server.yaml that puts the nginx configuration, self-signed certificate and docker-compose.yml to localhost to the appropriate place
- Whenever the nginx config file is changed or docker-compose.yml file is updated, it'll trigger a restart of docker-compose so the services that host the actual web server running gets restarted with appropriate changes being made

AWS Side: Deployed an AWS cloudformation template called "Deploy_simple_site_using_AWS.json" that does the following:
- Create a VPC, the Internet Gateway and the connection between them
- Add a subnet, securitygroup to the template and all the relevant links necessary between these components
- Adds parameters to the template so the values can be inputted when you create the stack later on for things like database login info, etc
- When the stack is deployed, the end result is that it sets up a wordpress site automatically using VPC, subnets and relevant gateways and security groups

Incomplete:
- Not able to integrate ansible to auto deploy the AWS cloudformation stack to AWS

Contents of directory "sandbox"
- I wrote a simple go program that starts an http server at port 6000 that just prints "REAN CLOUD CHALLENGE" and then packaged it to be a docker image
- The contents in ./sandbox/nginx/ is automatically managed by ansible playbook above. The docker container that runs nginx just uses these configs for its own deployment
- ./sandbox/docker-compose.yml is the main file. It contains two docker containers(our website docker image and nginx image) that are linked together
- The actual nginx config located at ./sandbox/nginx/webserver.conf is the one that handles all the routing. Anytime the user tries to access the website at a particular URL(in our case, it's http://localhost:8000), it just redirects to https). If, instead the user directly goes to https://localhost:9000, it works as expected

Pipeline:
- Run ansible playbook using "ansible-playbook deploy_server.yml"
- This will start docker containers and start running our website via a docker container
- The goal was to then combine this part of the configuration management part with that of AWS cloudformation autodeploy but because this is my first even diving into AWS, I was unable to get this in the amount of time I worked(Around 6-7 hours)

Assumptions:
- localhost already has docker installed and ansible installed
- I ran this on a centos VM in Virtualbox so I had to configure additional settings via virtualbox settings so i could route from my internet browser on my windows box(host) to the docker VM(that's hosted in centos VM that is in turn hosted in virtualbox)
- Virtualbox > Centos 7 VM(right click) > Settings > Network > Port Forwarding and after this, i had to forward port 8000:8000 and 443:9000 respectively

Commands executed in order:
- yum install ansible
- cd ./sandbox/webserver; docker build -t webserver .
- cd ./ansible; ansible-playbook deploy_server.yaml 
- Go to your internet browser like chrome on your host box(for me, it was my windows destkop) and then go to "localhost:8000". What this essentially does is routes the traffic from my windows box to my centos VM to port 8000(because of the mapping i did in my virtualbox settings) and then nginx routes this port to 443(HTTPS)
- You can also go to localhost:9000 directly and this will also get routed to port 443(HTTPS) as well
- To test if port 443 is listening, just run ./test/check_port.sh
- For deploying the cloudformation template, simply go to aws console, cloudformation then upload the template and deploy the stack
